# If not running interactively, don't do anything
[ -z "$PS1" ] && return

HISTCONTROL=ignoreboth

# **
shopt -s globstar
# don't override history file
shopt -s histappend
# update $LINES and $COLUMNS after each command
shopt -s checkwinsize
# no * in arglist if no match
# shopt -s nullglob

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# autologout tty
[[ $(tty) =~ /dev/tty[1-6] ]] && TMOUT=600

# shellcheck disable=1091
# source /etc/profile.d/undistract-me.sh

# _x='\e[0m\]'
# _W='\e[1m\]'
# _g='\e[37m\]'
prompt_command() {
	# shellcheck disable=SC2181
	if [ $? -eq 0 ]; then
		local DOLLAR=\$
	else
		local DOLLAR=!
	fi
	local x='\[\033[0m\]'
	local BILA='\[\033[1m\]'
	local seda='\[\033[37m\]'

	# PS1="$_W\\u@\\h$_x:$_g\\w$_x$_W$DOLLAR $_x"
	# PS1="${debian_chroot:+($debian_chroot)}$BILA\u@\h$x:$seda\w$x$BILA$DOLLAR $x" 
	PS1="$BILA\\u@\\h$x:$seda\\w$x$BILA$DOLLAR $x" 
}
# PROMPT_COMMAND="${PROMPT_COMMAND};prompt_command"
PROMPT_COMMAND="prompt_command;${PROMPT_COMMAND}"

#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#    . /etc/bash_completion
#fi
if [ -f /usr/share/bash-completion/bash_completion ] && ! shopt -oq posix; then
	# shellcheck disable=SC1091
    . /usr/share/bash-completion/bash_completion
fi
# MY_BASH_COMPLETION_COMPAT_DIR=$HOME/bin/bash_completion.d
for i in "$HOME"/bin/bash_completion.d/*; do
	# shellcheck disable=SC1090
	. "$i"
done
eval "$(pandoc --bash-completion)"
eval "$(kitty + complete setup bash)"

eval "$(dircolors -b)"


# aliases
alias ls='ls --color=auto'
alias ll='ls -lF'
alias grep='grep --color=auto --perl-regexp'
alias curl='curl --silent'
alias cal='ncal -bM'
alias icat='kitty +kitten icat'

alias hcat='highlight -O xterm256'
alias anticensore='sed y/aeopcyxABEKMHOPCTX/аеорсухАВЕКМНОРСТХ/'
alias rot13='tr A-Za-z N-ZA-Mn-za-m'
alias qr='ttyqr -b'
alias curdate='date +%F'
alias unzip-cp852='~/bin/crap/unzip -O CP852'
alias create-tar='tar --create --auto-compress --file'
alias extract-tar='tar --extract --auto-compress --file'

alias tmp='cd /tmp'
alias ptp='if jobs "%ptpython"; then fg "%ptpython"; else ptpython; fi'
alias node='if jobs "%node"; then fg "%node"; else node; fi'

alias ap='sudo create_ap --no-virt "$WIRELESS" "$WIRED" "$HOSTNAME" asdfasdf'

alias primary='xrandr --output "$DSP0" --auto --output "$DSP1" --off'
alias secondary='xrandr --output "$DSP0" --off --output "$DSP1" --auto'
alias above='xrandr --output "$DSP0" --auto --output "$DSP1" --above "$DSP0"'
alias above720='xrandr --output "DSP0" --auto --output "$DSP1" --mode 1280x720 --above "$DSP0"'

# alias droidmount='echo admin | sshfs root@droid.home:/mnt/sdcard /mnt/droid/ -o password_stdin'
# alias pushpics='sshpass -p admin rsync -rh --progress --delete -vze ssh ~/pics/DCIM/ root@droid.home:/mnt/sdcard/DCIM/Camera/'
# alias pullpics='sshpass -p admin rsync -rh --progress --chmod=D755,F644 -vze ssh root@droid.home:/mnt/sdcard/DCIM/Camera/ ~/pics/DCIM/'

alias rpimount='sshfs pi@rpi.home:/media/sda1 /mnt/rpi/'
alias check-sda1='pumount /dev/sda1 && sudo fsck -y /dev/sda1; pmount /dev/sda1'

alias cs='setxkbmap uscz'
alias sk='setxkbmap ussk'
alias de='setxkbmap usde'
alias es='setxkbmap uses'
alias fr='setxkbmap usfr'
alias ru='setxkbmap -device 3 -layout uscz; setxkbmap -device 14 -layout ruus'
alias boxdrawing='setxkbmap boxdrawing'

alias eshop='cd ~/work/eshop'
alias plz-send-artifacts='rsync -ah --progress -vze ssh fareoneshop*.tgz install_*.sh plz:~'
alias frontend-incver='incversion.sh /VERSION/ ~/work/eshop/frontend/src/app/version.js'
alias backend-incver='incversion.sh /__version__/ ~/work/eshop/backend/foebackend/__init__.py'
alias workers-incver='incversion.sh /version/ ~/work/eshop/workers/setup.py'

# edit command in vim with [Insert]
bind '"\e[2~":edit-and-execute-command'

# functions

n() {
	nohup "$@" &> /dev/null &
}

rpisend() {
	rsync -ah --progress -vze ssh "$@" pi@rpi.home:/media/sda1/downloads/
}

# droidsend() {
# 	sshpass -p admin rsync -rh --progress -vze ssh "$@" root@droid.home:/mnt/sdcard-ext/
# }

M() {
	aptitude search "!~M~i$*"
}

dvb() {
	mpv --deinterlace=yes "dvb://$*"
}

ranger-cd() {
    tmp="$(mktemp)"
    ranger --choosedir="$tmp" "$@"
    if [ -f "$tmp" ]; then
		cd -- "$(cat "$tmp")" || true
	fi
    rm -f -- "$tmp"
}
bind '"\C-o":"ranger-cd\C-m"'

winpython() {
	PYTHONIOENCODING=utf8 \
	WINEDEBUG=-all \
	wine64 'C:\python3\python.exe' "$@"		# 2>/dev/null
}

eshop3() {
	if [ "$#" -eq 0 ]; then
		cd ~/work/eshop3/ || true
	fi

	~/bin/eshop3 "$@"
}

# free ctrl combinations:
# ^G (abort search)
# ^Q (resume output)
# ^S (stop output, search forward)
# ^T (transpose chars)
# ^Y (paste)
